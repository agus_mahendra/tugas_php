<?php
    
    if(isset($_POST["submit"])){
        $nama = $_POST["Nama"];
        $mapel = $_POST["Mapel"];
        $uts = $_POST["UTS"];
        $uas = $_POST["UAS"];
        $tugas = $_POST["Tugas"];

        $nilaiuts = $uts  * 0.35;
        $nilaiuas = $uas * 0.5;
        $nilaitugas = $tugas * 0.15;

        $total = $nilaiuts + $nilaiuas +$nilaitugas;

        //Let's Grading
        if($total>=90 && $total <= 100){
            $grade = "A";
        }else if($total>70 && $total < 90){
            $grade = "B";
        }else if($total>50 && $total <= 70){
            $grade = "C";
        }else if($total<=50){
            $grade = "D";
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Input Nilai</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
        <div class="border border-primary rounded mt-3">
            <div class="container p-3 my-3 bg-primary text-white">
            <center>
            <h1>Formulir Input Nilai Siswa</h1>
                <p>Masukkan nilai dan data siswa pada formulir di bawah </p>
            </center>
            </div>
            <form action="" method="post">
            <!-- Form nama -->
            <div class="form1">
                <label for="Nama">Nama :</label><br>
                <input type="text" class="form-control" name="Nama" required><br>
            </div>
                
            <!-- Form mata kuliah -->
            <div class="form2">
                <label for="mapel">Mata Pelajaran :</label><br>
                <input type="text" class="form-control" name="Mapel" required><br>
            </div>

            <!-- Form Nilai-Nilai -->
                    <label for="Nilai UTS">Nilai UTS :</label><br>
                    <input type="text" class="form-control" name="UTS" required><br>
                    <label for="Nilai UAS">Nilai UAS :</label><br>
                    <input type="text" class="form-control" name="UAS" required><br>
                    <label for="Nilai Tugas">Nilai Tugas :</label><br>
                    <input type="text" class="form-control" name="Tugas" required><br>
            <center>
            <input type="submit" class="btn btn-primary" name="submit" value='SUBMIT' >
            </center>
            </form><br>
        </div>
    </div>
    <!-- Output Process -->
    <?php if(isset($_POST["submit"])) : ?>
    <div class="container mt-3">
                <div class="alert alert-success">
                <?php
                    echo "Nama Siswa : $nama <br>";
                    echo "Mata Pelajaran : $mapel <br>";
                    echo "Nilai UTS : $uts <br>";
                    echo "Nilai UAS : $uas <br>";
                    echo "Nilai Tugas : $tugas <br>";
                    echo "Total Nilai : $total <br>";
                    echo "<h2>Grade : $grade </h2>";
                ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
</body>
</html>